#KJEmoji
一款Android的Emoji表情控件
---

# 公告
KJEmoji的实现方式已经过时，推荐你看我的新作品，基于系统层的实现，效率更高，使用更灵活方便[EmojiChat](https://github.com/kymjs/EmojiChat)

## 功能简介
最大的优势在于其灵活性，支持单种类的表情或分类Emoji表情以及自定义表情的显示，可以使用在任何EditText的输入，可以在任何TextView中显示。<br>
你可以自由配置每个Emojicon图标所代表的文字，示例：[DisplayRules](https://git.oschina.net/kymjs/KJEmoji/blob/master/KJEmojiLibrary/src/org/kymjs/emoji/model/DisplayRules.java)<br>

KJEMOJI2(0, 2, R.drawable.smiley_1, "[02]");<br>
> 表示这个属性叫KJEMOJI2，<br>
> 在ViewPager中的第0页，<br>
> 它对应的值是2(这个是为了方便区分，如果不需要可以传任意值)，<br>
> 资源名是smiley_1，<br>
> 当复制时(或在服务器传输时)替代的文字是"[02]"。<br>

在解析时，同样支持根据自定义符号解析，比如你可以以[02]表示一个表情，也可以以":hello:"表示一个表情。

## 使用方法

###基础用法
```java
public class MainActivity extends FragmentActivity implements
        OnSendClickListener {

    private KJEmojiFragment emojiFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emojiFragment = new KJEmojiFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, emojiFragment).commit();
    }

    @Override
    public void onClickSendButton(Editable str) {
        Toast.makeText(this, str.toString(), Toast.LENGTH_SHORT).show();
    }
}
```

###在TextView中显示

```java
/**
 * @param res Resource对象
 * @param str 要显示的包含Emoji表情字符的字符串
 * @param flagStart 表情解析的起始标记
 * @param flagEnd 表情解析的结束标记
 */
InputHelper.displayEmoji(Resources res, String str,
            String flagStart, String flagEnd);
```

### 更多高级配置
参看代码注释[KJEmojiConfig](https://git.oschina.net/kymjs/KJEmoji/blob/master/KJEmojiLibrary/src/org/kymjs/emoji/model/KJEmojiConfig.java)

### 截图(左侧为多种Emoji表情，右侧为仅一种Emoji表情的显示)
<a href="http://www.kymjs.com/"><img src="http://www.kymjs.com/image/kjemoji/KJEmoji.gif" height="400" width="240" alt="开源实验室"/></a>
<a href="http://www.kymjs.com/"><img src="http://www.kymjs.com/image/kjemoji/KJEmoji.png" height="400" width="240" alt="开源实验室"/></a>