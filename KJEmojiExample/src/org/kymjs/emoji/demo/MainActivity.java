package org.kymjs.emoji.demo;

import org.kymjs.emoji.KJEmojiFragment;
import org.kymjs.emoji.OnSendClickListener;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.view.KeyEvent;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements
        OnSendClickListener {

    private KJEmojiFragment emojiFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        emojiFragment = new KJEmojiFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.root, emojiFragment).commit();
    }

    @Override
    public void onClickSendButton(Editable str) {
        Toast.makeText(this, str.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && emojiFragment.isShowEmojiKeyBoard()) {
            emojiFragment.hideAllKeyBoard();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
